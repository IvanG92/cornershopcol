# README #

Este proyecto es el resultado de la introducción al desarrollo de aplicaciones Móviles con Kotlin,
ofrecido por el MINTIC bajo su iniciativa de mejorar el talento en TI.

### Proyecto

* Esqueleto funcional de una tienda de productos
* Versión 1.0.0
* [Código](https://bitbucket.org/IvanG92/cornershopcol)

### How do I get set up? ###

* Configuración

En el local.properties crear la variable GOOGLE_MAPS_API_KEY con el key entregado por google para el
consumo del API