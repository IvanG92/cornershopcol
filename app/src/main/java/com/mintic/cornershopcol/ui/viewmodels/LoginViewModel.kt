package com.mintic.cornershopcol.ui.viewmodels

import android.graphics.Bitmap
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.FirebaseAuthInvalidUserException
import com.google.firebase.auth.FirebaseUser
import com.mintic.cornershopcol.data.repositories.UserRepository
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await

class LoginViewModel(private val repo:UserRepository):ViewModel() {
    private var _user: MutableLiveData<FirebaseUser?> = MutableLiveData()
    val user get()= _user

    private var _error: MutableLiveData<String> = MutableLiveData()
    val error:LiveData<String> get() = _error

    fun register(email:String,password:String,username:String){
        viewModelScope.launch {
            try {
                _user.postValue(repo.register(email,password,username))
            }catch (e: Error){
                _error.postValue(e.message!!)
            }
        }
    }

    fun loggedIn(){
        viewModelScope.launch {
            _user.postValue(repo.loggedIn())
        }
    }

    fun login(email:String, password: String){
        viewModelScope.launch {
           try {
               _user.postValue(repo.login(email,password))
           }catch (e:Error){
               _error.postValue(e.message!!)
           }
        }
    }

    fun logOut(){
        viewModelScope.launch {
            _user.postValue(repo.logOut())
        }
    }

    fun uploadImage(bitmap: Bitmap){
        viewModelScope.launch {
            _user.postValue(repo.uploadImage(bitmap))
        }
    }

}