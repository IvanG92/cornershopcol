package com.mintic.cornershopcol.ui.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.gms.common.util.CollectionUtils.listOf
import com.mintic.cornershopcol.R
import com.mintic.cornershopcol.ui.adapters.ProductAdapter
import com.mintic.cornershopcol.data.models.Product
import com.mintic.cornershopcol.databinding.FragmentCatalogueBinding
import com.mintic.cornershopcol.ui.listeners.OnProductListener
import com.mintic.cornershopcol.ui.viewmodels.ProductViewModel
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class CatalogueFragment : Fragment() {

    private var _binding: FragmentCatalogueBinding? = null
    private val binding get() = _binding!!

    private val productViewModel: ProductViewModel by sharedViewModel()

    private lateinit var productAdapter: ProductAdapter
    private lateinit var productManager: GridLayoutManager

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
          // Inflate the layout for this fragment
//        return inflater.inflate(R.layout.fragment_catalogue, container, false)
        _binding = FragmentCatalogueBinding.inflate(inflater,container, false)
        return binding.root
    }

    override fun onStart() {
        super.onStart()
        productAdapter = ProductAdapter (
            listOf()
        )
        productAdapter.listener = object: OnProductListener{
            override fun onClick(product: Product) {
                Log.d("CLICK",product.name!!)
                productViewModel.selectProduct(product)
                findNavController().navigate(R.id.action_catalogueFragment_to_productDetailFragment)
            }
        }
        productViewModel.loadProducts()

        productManager = GridLayoutManager(requireContext(),2)
        binding.productRecycler.adapter = productAdapter
        binding.productRecycler.layoutManager = productManager

        productViewModel.products.observe(viewLifecycleOwner, Observer { products ->
            productAdapter.newDataSet(products)
        })
    }
}