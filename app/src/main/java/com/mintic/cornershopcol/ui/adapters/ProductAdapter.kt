package com.mintic.cornershopcol.ui.adapters

import com.mintic.cornershopcol.ui.listeners.OnProductListener
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mintic.cornershopcol.data.models.Comment
import com.mintic.cornershopcol.data.models.Product
import com.mintic.cornershopcol.databinding.ItemProductBinding

class ProductAdapter(var items: List<Product>): RecyclerView.Adapter<ProductAdapter.ViewHolder>() {

    var listener: OnProductListener? = null

    class ViewHolder (val item: ItemProductBinding): RecyclerView.ViewHolder(item.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType:Int): ViewHolder {
        return ViewHolder(
            ItemProductBinding.inflate(LayoutInflater.from(parent.context),parent, false))
    }

    override fun onBindViewHolder(
        holder: ViewHolder,
        position: kotlin.Int
    ) {
        val item = items[position]
        holder.item.itemNameProduct.text = item.name
        holder.item.itemPriceProduct.text = item.price
        Glide.with(holder.itemView).load(item.image).into(holder.item.itemProductPhoto)
        holder.item.root.setOnClickListener{
            listener?.onClick(item)
        }
    }

    override fun getItemCount(): kotlin.Int {
       return items.size
    }

    fun newDataSet(products:List<Product>){
        items = products
        notifyDataSetChanged()
    }
}