package com.mintic.cornershopcol.ui

import android.app.Application
import com.google.android.gms.common.util.CollectionUtils.listOf
import com.mintic.cornershopcol.di.dataSourceModule
import com.mintic.cornershopcol.di.databaseModule
import com.mintic.cornershopcol.di.repoModule
import com.mintic.cornershopcol.di.viewModelModule
import kotlinx.coroutines.InternalCoroutinesApi
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App:Application() {
    @InternalCoroutinesApi
    override fun onCreate() {
        super.onCreate()
        startKoin{
            androidContext(this@App)
            modules(listOf(dataSourceModule, repoModule, viewModelModule, databaseModule))
        }
    }
}