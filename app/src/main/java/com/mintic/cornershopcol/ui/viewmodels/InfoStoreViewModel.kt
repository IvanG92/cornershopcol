package com.mintic.cornershopcol.ui.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

import androidx.lifecycle.viewModelScope
import com.mintic.cornershopcol.data.models.StoreInfo
import com.mintic.cornershopcol.data.repositories.StoreInfoRepository
import kotlinx.coroutines.launch

class InfoStoreViewModel(private val repo:StoreInfoRepository): ViewModel() {
    private var _info: MutableLiveData<StoreInfo> = MutableLiveData()
    val info: LiveData<StoreInfo> get() = _info

    fun loadStoreInfo(){
        viewModelScope.launch {
            val result = repo.loadStoreInfo()
            _info.postValue(result)
        }

    }
}