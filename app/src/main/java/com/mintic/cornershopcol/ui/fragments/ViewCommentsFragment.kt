package com.mintic.cornershopcol.ui.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.gms.common.util.CollectionUtils.listOf
import com.mintic.cornershopcol.ui.adapters.CommentAdapter
import com.mintic.cornershopcol.data.models.Comment
import com.mintic.cornershopcol.databinding.FragmentViewCommentsBinding
import com.mintic.cornershopcol.ui.listeners.OnCommentListener
import com.mintic.cornershopcol.ui.viewmodels.CommentViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class ViewCommentsFragment : Fragment() {

    private var _binding:FragmentViewCommentsBinding? = null
    private val binding get() = _binding!!

    private val commentViewModel: CommentViewModel by viewModel()

    private lateinit var commentAdapter: CommentAdapter
    private lateinit var commentManager: GridLayoutManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        // return inflater.inflate(R.layout.fragment_view_comments, container, false)
        _binding = FragmentViewCommentsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onStart() {
        super.onStart()
        commentAdapter = CommentAdapter (
            listOf()
        )
        commentAdapter.listener = object:OnCommentListener{
            override fun onClick(comment: Comment) {
                Log.d("CLICk",comment.name!!)
            }
        }

        commentViewModel.loadComments()

        commentManager = GridLayoutManager(requireContext(),1)
        binding.commentRecycler.adapter = commentAdapter
        binding.commentRecycler.layoutManager = commentManager

        commentViewModel.comments.observe(viewLifecycleOwner, Observer { comments ->
            commentAdapter.newDataSet(comments)
        })
    }
}