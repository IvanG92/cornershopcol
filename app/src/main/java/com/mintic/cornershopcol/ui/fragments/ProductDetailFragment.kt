package com.mintic.cornershopcol.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.mintic.cornershopcol.R
import com.mintic.cornershopcol.databinding.FragmentInfoStoreBinding
import com.mintic.cornershopcol.databinding.FragmentProductDetailBinding
import com.mintic.cornershopcol.databinding.FragmentSecondBinding
import com.mintic.cornershopcol.ui.viewmodels.ProductViewModel
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.core.component.getScopeName

class ProductDetailFragment : Fragment() {
    private var _binding: FragmentProductDetailBinding? = null

    private val binding get() = _binding!!

    private val productViewModel: ProductViewModel by sharedViewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
//        return inflater.inflate(R.layout.fragment_product_detail, container, false)
        _binding = FragmentProductDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onStart() {
        super.onStart()
        productViewModel.selected.observe(viewLifecycleOwner, Observer { product ->
            binding.productName.text = product.name
            binding.productPrice.text = product.price
            binding.productDescription.text = product.description
            if (product.image !== null) {
                Glide.with(binding.root)
                    .load(product.image)
                    .into(binding.productImage)
            } else {
                binding.productImage.setImageResource(R.drawable.ic_launcher_background)
            }
        })
    }
}