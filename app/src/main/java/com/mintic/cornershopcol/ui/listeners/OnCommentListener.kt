package com.mintic.cornershopcol.ui.listeners

import com.mintic.cornershopcol.data.models.Comment

interface OnCommentListener {
    fun onClick(comment: Comment)
}