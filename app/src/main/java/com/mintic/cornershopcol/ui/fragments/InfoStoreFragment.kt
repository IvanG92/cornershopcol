package com.mintic.cornershopcol.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.Glide
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.MarkerOptions
import com.mintic.cornershopcol.R
import com.mintic.cornershopcol.data.models.StoreInfo

import com.mintic.cornershopcol.databinding.FragmentInfoStoreBinding
import com.mintic.cornershopcol.ui.viewmodels.InfoStoreViewModel
import com.mintic.cornershopcol.utils.Constants
import org.koin.androidx.viewmodel.ext.android.viewModel

class InfoStoreFragment : Fragment() {
    private var _binding: FragmentInfoStoreBinding? = null
    private val binding get() = _binding!!
    private val infoStoreViewModel: InfoStoreViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentInfoStoreBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onStart() {
        super.onStart()

        infoStoreViewModel.loadStoreInfo()
        infoStoreViewModel.info.observe(viewLifecycleOwner, Observer { store ->
            binding.TextWhatsapp.setText(store.whatsapp)
            binding.TextPhone.setText(store.phone)
            binding.textEmail.setText(store.email)
            binding.TextPhone2.setText(store.smartphone)
            binding.direccion.setText(store.address)
            if (store.image !== null) {
                Glide.with(this)
                    .load(Constants.mapOfImages[store.image])
                    .into(binding.tienda)
            } else {
                binding.tienda.setImageResource(R.drawable.ic_launcher_background)
            }
            if (store.logo !== null) {
                Glide.with(this)
                    .load(Constants.mapOfImages[store.logo])
                    .into(binding.logo)
            } else {
                binding.logo.setImageResource(R.drawable.ic_launcher_background)
            }

            val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
            mapFragment?.getMapAsync(addMarker(store))
        })

        binding.TextWhatsapp.setTextIsSelectable(false)
        binding.TextPhone.setTextIsSelectable(false)
        binding.textEmail.setTextIsSelectable(false)
        binding.TextPhone2.setTextIsSelectable(false)
        binding.direccion.setTextIsSelectable(false)

        val whatsapp = R.drawable.whatsapp_30
        val phone = R.drawable.smartphone
        val phone2 = R.drawable.phone
        val email = R.drawable.arroba


        if (phone !== null) {
            Glide.with(this)
                .load(phone)
                .into(binding.phone)
        } else {
            binding.phone.setImageResource(R.drawable.ic_launcher_background)
        }
        if (phone2 !== null) {
            Glide.with(this)
                .load(phone2)
                .into(binding.phone2)
        } else {
            binding.phone2.setImageResource(R.drawable.ic_launcher_background)
        }
        if (email !== null) {
            Glide.with(this)
                .load(email)
                .into(binding.email)
        } else {
            binding.email.setImageResource(R.drawable.ic_launcher_background)
        }
        if (whatsapp !== null) {
            Glide.with(this)
                .load(whatsapp)
                .into(binding.whatsapp)
        } else {
            binding.whatsapp.setImageResource(R.drawable.ic_launcher_background)
        }
    }

    private fun addMarker(store: StoreInfo): OnMapReadyCallback {
        return OnMapReadyCallback { googleMap ->
            googleMap.addMarker(
                MarkerOptions().position(LatLng(store.latitude!!, store.longitude!!))
                    .title(store.name)
            )
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(LatLng(store.latitude!!, store.longitude!!)))
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


}