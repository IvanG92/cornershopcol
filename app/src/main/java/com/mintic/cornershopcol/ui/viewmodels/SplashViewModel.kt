package com.mintic.cornershopcol.ui.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.FirebaseUser
import com.mintic.cornershopcol.data.models.Comment
import com.mintic.cornershopcol.data.models.Product
import com.mintic.cornershopcol.data.models.StoreInfo
import com.mintic.cornershopcol.data.repositories.CommentRepository
import com.mintic.cornershopcol.data.repositories.ProductRepository
import com.mintic.cornershopcol.data.repositories.StoreInfoRepository
import com.mintic.cornershopcol.data.repositories.UserRepository
import kotlinx.coroutines.launch

class SplashViewModel(
    private val commentRepo: CommentRepository,
    private val productRepo: ProductRepository,
    private val storeRepo: StoreInfoRepository,
    private val userRepo: UserRepository
):ViewModel(){

    private var _user: MutableLiveData<FirebaseUser?> = MutableLiveData()
    val user: LiveData<FirebaseUser?> get() = _user

    fun insert(comments:List<Comment>, products: List<Product>, store: StoreInfo){
        viewModelScope.launch {
            commentRepo.insertComments(comments)
            productRepo.insertProducts(products)
            storeRepo.insertStore(store)
        }
    }

    fun loggedIn(){
        viewModelScope.launch {
            _user.postValue(userRepo.loggedIn())
        }
    }
}