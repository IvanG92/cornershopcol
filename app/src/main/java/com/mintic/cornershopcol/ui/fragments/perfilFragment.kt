package com.mintic.cornershopcol.ui.fragments

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.mintic.cornershopcol.databinding.FragmentPerfilBinding
import com.mintic.cornershopcol.ui.activities.LoginActivity
import com.mintic.cornershopcol.ui.activities.MainActivity
import com.mintic.cornershopcol.ui.viewmodels.LoginViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class perfilFragment : Fragment() {

    private var _binding: FragmentPerfilBinding? = null

    private val binding get() = _binding!!

    private val loginViewModel:LoginViewModel by viewModel()

    private val  REQUEST_CAMERA_PERMISSION = 1
    private val  REQUEST_IMAGE = 2

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentPerfilBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onStart() {
        super.onStart()
        checkPermission()

        loginViewModel.loggedIn()

        loginViewModel.user.observe(viewLifecycleOwner, Observer { user ->
            if(user == null){
                val intent = Intent(requireContext(), LoginActivity::class.java)
                startActivity(intent)
                requireActivity().finish()
            }else{
                binding.editPersonName.text = user!!.displayName
                if(user!!.photoUrl != null){
                    Glide.with(binding.root).load(user.photoUrl).into(binding.imageView)
                }
            }
        })

        binding.logoutButton.setOnClickListener{
            loginViewModel.logOut()
        }

        binding.imageView.setOnClickListener {
            Log.d("CLICk", "camera click")
            if(ContextCompat.checkSelfPermission(requireContext(),Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED){
                Intent(MediaStore.ACTION_IMAGE_CAPTURE).also{ intent ->
                    startActivityForResult(intent,REQUEST_IMAGE)
                    /*intent.resolveActivity(requireActivity().packageManager)?.also{

                    }*/
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == REQUEST_IMAGE){
                val bitmap = data?.extras?.get("data") as Bitmap
                binding.imageView.setImageBitmap(bitmap)
                loginViewModel.uploadImage(bitmap)
            }
        }
    }

    private fun checkPermission(){
        if(ContextCompat.checkSelfPermission(requireContext(),Manifest.permission.CAMERA)
            != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(requireActivity(), arrayOf(Manifest.permission.CAMERA),
                REQUEST_CAMERA_PERMISSION)
        }
    }
}