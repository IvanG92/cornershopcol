package com.mintic.cornershopcol.ui.activities

import android.animation.Animator
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import com.mintic.cornershopcol.R
import com.mintic.cornershopcol.data.models.Comment
import com.mintic.cornershopcol.data.models.Product
import com.mintic.cornershopcol.data.models.StoreInfo
import com.mintic.cornershopcol.databinding.ActivitySplashScreenBinding
import com.mintic.cornershopcol.ui.viewmodels.SplashViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class SplashScreenActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySplashScreenBinding
    private val splashViewModel: SplashViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivitySplashScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
    
    override fun onStart(){
        super.onStart()
        splashViewModel.loggedIn()
        splashViewModel.insert(
            listOf(
                Comment(
                    1,
                    name = "Andres 1",
                    image = "https://toppng.com/uploads/preview/flat-faces-icons-circle-persona-icon-115628952315akhsf8ncl.png",
                    message = "Mensaje 111"
                ),
                Comment(
                    2,
                    name = "Andres 2",
                    image = "https://toppng.com/uploads/preview/flat-faces-icons-circle-persona-icon-115628952315akhsf8ncl.png",
                    message = "Mensaje 222"
                ),
                Comment(
                    3,
                    name = "Andres 3",
                    image = "https://toppng.com/uploads/preview/flat-faces-icons-circle-persona-icon-115628952315akhsf8ncl.png",
                    message = "Mensaje 333"
                ),
                Comment(
                    4,
                    name = "Andres 4",
                    image = "https://toppng.com/uploads/preview/flat-faces-icons-circle-persona-icon-115628952315akhsf8ncl.png",
                    message = "Mensaje 444"
                )),
            listOf(
                Product(
                    1,
                    name = "Producto 1",
                    image = "https://www.costco.com.mx/medias/sys_master/products/hd5/h8e/52448503070750.jpg",
                    price = "111",
                    description = "Lorem ipsum dolor sit amet, mel ea nisl semper consequat. Graeco consetetur usu in, numquam salutatus ne eos. Ea pro praesent definitiones, eum at assum minimum appareat. Tota prima aliquid nam an, pri te oratio volumus, nam falli minimum temporibus an. Nec cu mandamus inimicus. Ad ferri fabellas mea, vix ea delectus delicata instructior."
                ),
                Product(
                    2,
                    name = "Producto 2",
                    image = "https://www.costco.com.mx/medias/sys_master/products/hd5/h8e/52448503070750.jpg",
                    price = "222",
                    description = "Lorem ipsum dolor sit amet, mel ea nisl semper consequat. Graeco consetetur usu in, numquam salutatus ne eos. Ea pro praesent definitiones, eum at assum minimum appareat. Tota prima aliquid nam an, pri te oratio volumus, nam falli minimum temporibus an. Nec cu mandamus inimicus. Ad ferri fabellas mea, vix ea delectus delicata instructior."
                ),
                Product(
                    3,
                    name = "Producto 3",
                    image = "https://www.costco.com.mx/medias/sys_master/products/hd5/h8e/52448503070750.jpg",
                    price = "333",
                    description = "Lorem ipsum dolor sit amet, mel ea nisl semper consequat. Graeco consetetur usu in, numquam salutatus ne eos. Ea pro praesent definitiones, eum at assum minimum appareat. Tota prima aliquid nam an, pri te oratio volumus, nam falli minimum temporibus an. Nec cu mandamus inimicus. Ad ferri fabellas mea, vix ea delectus delicata instructior."
                ),
                Product(
                    4,
                    name = "Producto 4",
                    image = "https://www.costco.com.mx/medias/sys_master/products/hd5/h8e/52448503070750.jpg",
                    price = "444",
                    description = "Lorem ipsum dolor sit amet, mel ea nisl semper consequat. Graeco consetetur usu in, numquam salutatus ne eos. Ea pro praesent definitiones, eum at assum minimum appareat. Tota prima aliquid nam an, pri te oratio volumus, nam falli minimum temporibus an. Nec cu mandamus inimicus. Ad ferri fabellas mea, vix ea delectus delicata instructior."
                )
            ), StoreInfo(
                1,
                name = "CornerStore",
                image = "R.drawable.grocery_store_thumb",
                description = "Descripion de la tienda",
                address = "Calle Falsa 123 No. 45 -67, Bogota, Colombia, Local 0000 Piso 2",
                whatsapp = "3211234567890",
                email = "cornershop@cornershop.com.co",
                phone = "3211234567890",
                smartphone = "3211234567890",
                logo = "R.drawable.logo"
            )


            )
        //binding.splashAnimation.imageAssetsFolder = "images"
        binding.splashAnimation.playAnimation()
        binding.splashAnimation.addAnimatorListener(object: Animator.AnimatorListener{
            override fun onAnimationStart(p0: Animator?) {
            }

            override fun onAnimationEnd(p0: Animator?) {
                splashViewModel.user.observe(this@SplashScreenActivity, Observer{ user->
                    if(user != null){
                        val intent = Intent(this@SplashScreenActivity, MainActivity::class.java)
                        startActivity(intent)
                    }else{
                        val intent = Intent(this@SplashScreenActivity, LoginActivity::class.java)
                        startActivity(intent)
                    }
                    finish()
                })
            }

            override fun onAnimationCancel(p0: Animator?) {
            }

            override fun onAnimationRepeat(p0: Animator?) {

            }

        })
    }
}