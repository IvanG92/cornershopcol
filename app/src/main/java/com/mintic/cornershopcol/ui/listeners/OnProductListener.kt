package com.mintic.cornershopcol.ui.listeners

import com.mintic.cornershopcol.data.models.Product

interface OnProductListener {
    fun onClick(product: Product)
}