package com.mintic.cornershopcol.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.mintic.cornershopcol.databinding.FragmentRegisterBinding
import com.mintic.cornershopcol.ui.viewmodels.LoginViewModel

import com.mintic.cornershopcol.utils.isValidEmail
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*

class RegisterFragment : Fragment() {

    private var _binding: FragmentRegisterBinding? = null
    private val binding get() = _binding!!

    private val loginViewModel: LoginViewModel by viewModel()

    override fun onCreateView(inflater: LayoutInflater,container: ViewGroup?,savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentRegisterBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onStart() {
        super.onStart()

        loginViewModel.error.observe(viewLifecycleOwner, androidx.lifecycle.Observer { error ->
            Toast.makeText(requireContext(),error,Toast.LENGTH_LONG).show()
        })

        loginViewModel.user.observe(viewLifecycleOwner, androidx.lifecycle.Observer { user ->
            Toast.makeText(requireContext(),"Usuario registrado",Toast.LENGTH_LONG).show()
            requireActivity().onBackPressed()
        })
        binding.registerUser.setOnClickListener{
            var isValid = true

            if(!binding.registerEmail.text.isValidEmail()){
                isValid = false
                binding.registerEmailLayout.error = "Correo Electronico invalido"
            }else{
                binding.registerEmailLayout.error = null
            }

            if(binding.registerPassword.text.toString().length < 6){
                isValid = false
                binding.registerPasswordLayout.error = "Contrasena invalida"
            }else{
                binding.registerPasswordLayout.error = null
            }

            if(binding.registerUsername.text.toString().length < 10){
                isValid = false
                binding.registerUsernameLayout.error = "Nombre invalido"
            }else{
                binding.registerUsernameLayout.error = null
            }

            if(isValid){
                loginViewModel.register(binding.registerEmail.text.toString(),
                    binding.registerPassword.text.toString(),
                    binding.registerUsername.text.toString())
            }
        }


    }
}