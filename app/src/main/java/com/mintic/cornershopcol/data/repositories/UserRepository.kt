package com.mintic.cornershopcol.data.repositories

import android.graphics.Bitmap
import com.google.firebase.auth.*
import com.google.firebase.auth.ktx.userProfileChangeRequest
import com.google.firebase.storage.StorageReference
import kotlinx.coroutines.tasks.await
import java.io.ByteArrayOutputStream

class UserRepository(private val dataSource: FirebaseAuth,private val dataSourceStorage: StorageReference) {

    fun loggedIn(): FirebaseUser?{
        return dataSource.currentUser
    }

    suspend fun register(email:String, password: String, username:String):FirebaseUser?{
        try {
            dataSource.createUserWithEmailAndPassword(email,password).await()
            val user = dataSource.currentUser
            val profileUpdate = userProfileChangeRequest {
                displayName = username
            }
            user!!.updateProfile(profileUpdate)
            return user
        }catch (e:FirebaseAuthUserCollisionException){
            throw Error("Correo en uso")
        }
    }

    suspend fun login(email:String, password: String):FirebaseUser?{
        try {
            dataSource.signInWithEmailAndPassword(email,password).await()
            return dataSource.currentUser
        }catch (e:FirebaseAuthInvalidCredentialsException){
            throw Error("Credenciales Invalidas")
        }catch (e:FirebaseAuthInvalidUserException){
            throw Error("Usuario incorrecto")
        }
    }

    suspend fun logOut():FirebaseUser?{
        dataSource.signOut()
        return null
    }

    suspend fun uploadImage(bitmap: Bitmap): FirebaseUser?{
        val baos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG,100, baos)
        val data = baos.toByteArray()
        val user = dataSource.currentUser
        val profileRef = dataSourceStorage.child("${user!!.uid}.jpg")
        profileRef.putBytes(data).await()
        val uri = profileRef.downloadUrl.await()
        val profileUpdate = userProfileChangeRequest {
            photoUri = uri
        }
        user!!.updateProfile(profileUpdate).await()
        return user
    }
}