package com.mintic.cornershopcol.data.mocks

import com.mintic.cornershopcol.R
import com.mintic.cornershopcol.data.models.StoreInfo

class StoreInfoMock() {
    fun loadStoreInfo():StoreInfo {
        return StoreInfo(
            1,
            name = "CornerStore",
            image = "R.drawable.grocery_store_thumb",
            description = "Descripion de la tienda",
            address = "Calle Falsa 123 No. 45 -67, Bogota, Colombia, Local 0000 Piso 2",
            whatsapp = "3211234567890",
            email = "cornershop@cornershop.com.co",
            phone = "3211234567890",
            smartphone = "3211234567890",
            logo = "R.drawable.logo",
            latitude = 0.0,
            longitude = 0.0
        )
    }


}