package com.mintic.cornershopcol.data.databases.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.mintic.cornershopcol.data.models.StoreInfo

@Dao
interface StoreDao {
    @Insert
    suspend fun insertStore(store:StoreInfo)

    @Query(value = "SELECT * FROM StoreInfo LIMIT 1")
    suspend fun getStore(): StoreInfo?
}