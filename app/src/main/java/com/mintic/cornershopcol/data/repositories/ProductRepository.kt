package com.mintic.cornershopcol.data.repositories

import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.mintic.cornershopcol.data.databases.dao.ProductDao
import com.mintic.cornershopcol.data.mocks.ProductMock
import com.mintic.cornershopcol.data.models.Product
import com.mintic.cornershopcol.utils.Constants
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.coroutines.tasks.await

//class ProductRepository(private val dataSource: ProductMock) {
//class ProductRepository(private val dataSource: ProductDao) {
class ProductRepository(private val dataSource: ProductDao, private val dataSourceFirebase: FirebaseFirestore) {
    private val db: CollectionReference = dataSourceFirebase.collection(Constants.PRODUCT_COLLECTION)

    suspend fun loadProducts():List<Product>{
        /*return withContext(Dispatchers.IO){
//            dataSource.loadProducts()
            dataSource.getAllProducts()
        }*/
        val snapshot = db.get().await()
        return snapshot.toObjects(Product::class.java)
    }

    suspend fun insertProducts(products:List<Product>){
        withContext(Dispatchers.IO){
            val temp = dataSource.getAllProducts()
            if(temp.isEmpty()){
                dataSource.insertProducts(products)
            }
        }
    }
}