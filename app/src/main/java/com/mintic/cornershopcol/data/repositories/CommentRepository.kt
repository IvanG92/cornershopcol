package com.mintic.cornershopcol.data.repositories

import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.mintic.cornershopcol.data.databases.dao.CommentDao
import com.mintic.cornershopcol.data.models.Comment
import com.mintic.cornershopcol.utils.Constants
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext

//class CommentRepository(private val dataSource: CommentMock) {
//class CommentRepository(private val dataSource: CommentDao) {
class CommentRepository(private val dataSource: CommentDao, private val dataSourceFirebase:FirebaseFirestore) {

    private val db: CollectionReference = dataSourceFirebase.collection(Constants.COMMENT_COLLECTION)

    suspend fun loadComments(): List<Comment>{
        /* return withContext(Dispatchers.IO){
          dataSource.getAllComments()
      }*/

        val snapshot = db.get().await()
        return snapshot.toObjects(Comment::class.java)
    }

    suspend fun insertComments(comments:List<Comment>){
        withContext(Dispatchers.IO){
            val temp = dataSource.getAllComments()
            if(temp.isEmpty()){
                dataSource.insertComments(comments)
            }
        }
    }


}