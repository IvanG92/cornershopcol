package com.mintic.cornershopcol.data.mocks

import com.mintic.cornershopcol.data.models.Product

class ProductMock {
    fun loadProducts():List<Product>{
        return listOf(
            Product(
                1,
                name = "Producto 1",
                image = "https://www.costco.com.mx/medias/sys_master/products/hd5/h8e/52448503070750.jpg",
                price = "111",
                description = "Lorem ipsum dolor sit amet, mel ea nisl semper consequat. Graeco consetetur usu in, numquam salutatus ne eos. Ea pro praesent definitiones, eum at assum minimum appareat. Tota prima aliquid nam an, pri te oratio volumus, nam falli minimum temporibus an. Nec cu mandamus inimicus. Ad ferri fabellas mea, vix ea delectus delicata instructior."
            ),
            Product(
                2,
                name = "Producto 2",
                image = "https://www.costco.com.mx/medias/sys_master/products/hd5/h8e/52448503070750.jpg",
                price = "222",
                description = "Lorem ipsum dolor sit amet, mel ea nisl semper consequat. Graeco consetetur usu in, numquam salutatus ne eos. Ea pro praesent definitiones, eum at assum minimum appareat. Tota prima aliquid nam an, pri te oratio volumus, nam falli minimum temporibus an. Nec cu mandamus inimicus. Ad ferri fabellas mea, vix ea delectus delicata instructior."
            ),
            Product(
                3,
                name = "Producto 3",
                image = "https://www.costco.com.mx/medias/sys_master/products/hd5/h8e/52448503070750.jpg",
                price = "333",
                description = "Lorem ipsum dolor sit amet, mel ea nisl semper consequat. Graeco consetetur usu in, numquam salutatus ne eos. Ea pro praesent definitiones, eum at assum minimum appareat. Tota prima aliquid nam an, pri te oratio volumus, nam falli minimum temporibus an. Nec cu mandamus inimicus. Ad ferri fabellas mea, vix ea delectus delicata instructior."
            ),
            Product(
                4,
                name = "Producto 4",
                image = "https://www.costco.com.mx/medias/sys_master/products/hd5/h8e/52448503070750.jpg",
                price = "444",
                description = "Lorem ipsum dolor sit amet, mel ea nisl semper consequat. Graeco consetetur usu in, numquam salutatus ne eos. Ea pro praesent definitiones, eum at assum minimum appareat. Tota prima aliquid nam an, pri te oratio volumus, nam falli minimum temporibus an. Nec cu mandamus inimicus. Ad ferri fabellas mea, vix ea delectus delicata instructior."
            )
        )
    }
}