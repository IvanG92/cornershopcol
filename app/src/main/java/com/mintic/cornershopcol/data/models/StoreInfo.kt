package com.mintic.cornershopcol.data.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class StoreInfo(
    @PrimaryKey var id: Int? = null,
    var name: String? = null,
    var image: String? = null,
    var address: String? = null,
    var description: String? = null,
    var whatsapp: String? = null,
    var email: String? = null,
    var phone: String? = null,
    var smartphone: String? = null,
    var logo: String? = null,
    var latitude: Double? = null,
    var longitude: Double? = null
)
