package com.mintic.cornershopcol.data.databases.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.mintic.cornershopcol.data.models.Product
import com.mintic.cornershopcol.data.models.StoreInfo

@Dao
interface ProductDao {
    @Insert
    suspend fun insertProducts(products: List<Product>)

    @Query(value = "SELECT * FROM Product")
    suspend fun getAllProducts(): List<Product>
}