package com.mintic.cornershopcol.data.mocks

import com.google.android.gms.common.util.CollectionUtils
import com.mintic.cornershopcol.data.models.Comment

class CommentMock {
    fun loadComments():List<Comment>{
        return listOf(
            Comment(
                1,
                name = "Andres 1",
                image = "https://toppng.com/uploads/preview/flat-faces-icons-circle-persona-icon-115628952315akhsf8ncl.png",
                message = "Mensaje 111"
            ),
            Comment(
                2,
                name = "Andres 2",
                image = "https://toppng.com/uploads/preview/flat-faces-icons-circle-persona-icon-115628952315akhsf8ncl.png",
                message = "Mensaje 222"
            ),
            Comment(
                3,
                name = "Andres 3",
                image = "https://toppng.com/uploads/preview/flat-faces-icons-circle-persona-icon-115628952315akhsf8ncl.png",
                message = "Mensaje 333"
            ),
            Comment(
                4,
                name = "Andres 4",
                image = "https://toppng.com/uploads/preview/flat-faces-icons-circle-persona-icon-115628952315akhsf8ncl.png",
                message = "Mensaje 444"
            )
        )
    }
}