package com.mintic.cornershopcol.data.databases.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.mintic.cornershopcol.data.models.Comment
import com.mintic.cornershopcol.data.models.StoreInfo

@Dao
interface CommentDao {
    @Insert
    suspend fun insertComments(comments: List<Comment>)

    @Query(value = "SELECT * FROM Comment")
    suspend fun getAllComments(): List<Comment>
}