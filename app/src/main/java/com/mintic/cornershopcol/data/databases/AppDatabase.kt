package com.mintic.cornershopcol.data.databases

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.mintic.cornershopcol.data.databases.dao.CommentDao
import com.mintic.cornershopcol.data.databases.dao.ProductDao
import com.mintic.cornershopcol.data.databases.dao.StoreDao
import com.mintic.cornershopcol.data.models.Comment
import com.mintic.cornershopcol.data.models.Product
import com.mintic.cornershopcol.data.models.StoreInfo
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.internal.synchronized

@Database(entities = [Comment::class,Product::class,StoreInfo::class],version = 1)
abstract class AppDatabase:RoomDatabase() {
    abstract fun commentDao(): CommentDao
    abstract fun productDao(): ProductDao
    abstract fun storeInfoDao(): StoreDao

    companion object{
        @Volatile
        private var instance: AppDatabase? = null

        @InternalCoroutinesApi
        fun getInstance(context: Context): AppDatabase{
            if(instance == null){
                synchronized(this){
                    instance = Room.databaseBuilder(context.applicationContext, AppDatabase::class.java, "example").build()
                }
            }
            return instance!!
        }
    }
}