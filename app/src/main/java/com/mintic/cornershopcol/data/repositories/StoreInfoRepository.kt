package com.mintic.cornershopcol.data.repositories

import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.mintic.cornershopcol.data.databases.dao.StoreDao
import com.mintic.cornershopcol.data.mocks.StoreInfoMock
import com.mintic.cornershopcol.data.models.Product
import com.mintic.cornershopcol.data.models.StoreInfo
import com.mintic.cornershopcol.utils.Constants
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.coroutines.tasks.await

//class StoreInfoRepository(private val dataSource: StoreInfoMock) {
//class StoreInfoRepository(private val dataSource: StoreDao) {
class StoreInfoRepository(private val dataSource: StoreDao, private val dataSourceFirebase: FirebaseFirestore) {
    private val db: CollectionReference = dataSourceFirebase.collection(Constants.STORE_COLLECTION)

    suspend fun loadStoreInfo():StoreInfo{
        /*return withContext(Dispatchers.IO){
            dataSource.getStore()!!
        }*/
        val snapshot = db.get().await()
        return snapshot.toObjects(StoreInfo::class.java)[0]
    }

    suspend fun insertStore(store:StoreInfo){
        withContext(Dispatchers.IO){
            val temp = dataSource.getStore()
            if(temp == null){
                dataSource.insertStore(store)
            }
        }
    }
}