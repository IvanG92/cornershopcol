package com.mintic.cornershopcol.utils

import com.mintic.cornershopcol.R

class Constants {
    companion object{
        val COMMENT_COLLECTION="Comments"
        val PRODUCT_COLLECTION="Products"
        val STORE_COLLECTION="Store"
        val mapOfImages = mutableMapOf("R.drawable.grocery_store_thumb" to R.drawable.grocery_store_thumb, "R.drawable.logo" to R.drawable.logo)
    }
}