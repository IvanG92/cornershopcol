package com.mintic.cornershopcol.di

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.mintic.cornershopcol.data.mocks.CommentMock
import com.mintic.cornershopcol.data.mocks.ProductMock
import com.mintic.cornershopcol.data.mocks.StoreInfoMock
import org.koin.dsl.module

val dataSourceModule = module {
    single{StoreInfoMock()}
    single{CommentMock()}
    single{ ProductMock() }
    single{ FirebaseFirestore.getInstance()}
    single{ FirebaseAuth.getInstance()}
    single{ FirebaseStorage.getInstance().getReference()}
}