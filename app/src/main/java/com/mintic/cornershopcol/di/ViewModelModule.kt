package com.mintic.cornershopcol.di

import com.mintic.cornershopcol.ui.viewmodels.*
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { InfoStoreViewModel(get())}
    viewModel { CommentViewModel(get())}
    viewModel { ProductViewModel(get())}
    viewModel { SplashViewModel(get(),get(),get(),get())}
    viewModel { LoginViewModel(get())}
}