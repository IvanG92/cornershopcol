package com.mintic.cornershopcol.di

import com.mintic.cornershopcol.data.databases.AppDatabase
import kotlinx.coroutines.InternalCoroutinesApi
import org.koin.dsl.module

@InternalCoroutinesApi
val databaseModule = module {
    single{AppDatabase.getInstance(get())}
    single{get<AppDatabase>().commentDao()}
    single{get<AppDatabase>().productDao()}
    single{get<AppDatabase>().storeInfoDao()}
}