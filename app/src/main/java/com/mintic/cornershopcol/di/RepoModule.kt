package com.mintic.cornershopcol.di

import com.mintic.cornershopcol.data.repositories.CommentRepository
import com.mintic.cornershopcol.data.repositories.ProductRepository
import com.mintic.cornershopcol.data.repositories.StoreInfoRepository
import com.mintic.cornershopcol.data.repositories.UserRepository
import org.koin.dsl.module

val repoModule = module {
    /*single { StoreInfoRepository(get())}
    single { CommentRepository(get())}
    single { ProductRepository(get())}*/

    single { StoreInfoRepository(get(),get())}
    single { CommentRepository(get(),get())}
    single { ProductRepository(get(),get())}
    single { UserRepository(get(),get())}
}